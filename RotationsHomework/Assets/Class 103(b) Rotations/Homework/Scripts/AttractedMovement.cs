﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Homework
{

    public enum AroundAxis { X, Y, Z };

    public struct Matrix3x3
    {

        public float
            m00, m01, m02,
            m10, m11, m12,
            m20, m21, m22;


        public static Matrix3x3 Rotation(float angleInDeg, AroundAxis axis)
        {
            Matrix3x3 result = new Matrix3x3();
            float angleInRad = angleInDeg * Mathf.Deg2Rad;
            float cosA = Mathf.Cos(angleInRad);
            float sinA = Mathf.Sin(angleInRad);

            switch (axis)
            {
                case AroundAxis.X:
                    result.m00 = 1;
                    result.m01 = 0;
                    result.m02 = 0;

                    result.m10 = 0;
                    result.m11 = cosA;
                    result.m12 = -sinA;

                    result.m20 = 0;
                    result.m21 = sinA;
                    result.m22 = cosA;
                    break;
                case AroundAxis.Y:
                    result.m00 = cosA;
                    result.m01 = 0;
                    result.m02 = sinA;

                    result.m10 = 0;
                    result.m11 = 1;
                    result.m12 = 0;

                    result.m20 = -sinA;
                    result.m21 = 0;
                    result.m22 = cosA;
                    break;
                case AroundAxis.Z:
                    result.m00 = cosA;
                    result.m01 = -sinA;
                    result.m02 = 0;

                    result.m10 = sinA;
                    result.m11 = cosA;
                    result.m12 = 0;

                    result.m20 = 0;
                    result.m21 = 0;
                    result.m22 = 1;
                    break;
            }
            return result;
        }

        public static Vector3 operator *(Matrix3x3 A, Vector3 v)
        {
            Vector3 result;
            result.x = A.m00 * v.x + A.m01 * v.y + A.m02 * v.z;
            result.y = A.m10 * v.x + A.m11 * v.y + A.m12 * v.z;
            result.z = A.m20 * v.x + A.m21 * v.y + A.m22 * v.z;
            return result;
        }

        public static Matrix3x3 operator *(Matrix3x3 A, Matrix3x3 B)
        {
            Matrix3x3 result = new Matrix3x3
            {
                m00 = A.m00 * B.m00 + A.m01 * B.m10 + A.m02 * B.m20,
                m01 = A.m00 * B.m01 + A.m01 * B.m11 + A.m02 * B.m21,
                m02 = A.m00 * B.m02 + A.m01 * B.m12 + A.m02 * B.m22,

                m10 = A.m10 * B.m00 + A.m11 * B.m10 + A.m12 * B.m20,
                m11 = A.m10 * B.m01 + A.m11 * B.m11 + A.m12 * B.m21,
                m12 = A.m10 * B.m02 + A.m11 * B.m12 + A.m12 * B.m22,

                m20 = A.m20 * B.m00 + A.m21 * B.m10 + A.m22 * B.m20,
                m21 = A.m20 * B.m01 + A.m21 * B.m11 + A.m22 * B.m21,
                m22 = A.m20 * B.m02 + A.m21 * B.m12 + A.m22 * B.m22
            };


            return result;
        }

    }

    public class AttractedMovement : MonoBehaviour
    {

        public GameObject attractor;
        public float movementSpeed = 1f;
        public Vector3 imitatedRotationEuler;
        public Vector3 attractorRotation;
        public Vector3 debugVector;
        public Vector3 rotatedFwd;
        public Vector3 rotatedUp;
        // Use this for initialization
        void Start()
        {          

        }

        // Update is called once per frame
        void Update()
        {

            attractorRotation = attractor.transform.eulerAngles;
            Matrix3x3 rX = Matrix3x3.Rotation(attractorRotation.x, AroundAxis.X);
            Matrix3x3 rY = Matrix3x3.Rotation(attractorRotation.y, AroundAxis.Y);
            Matrix3x3 rZ = Matrix3x3.Rotation(attractorRotation.z, AroundAxis.Z);

            rotatedFwd = rZ * rY * rX * Vector3.forward;
            rotatedUp = rZ * rY * rX * Vector3.up;
            

            imitatedRotationEuler = rZ * rY * rX * this.transform.eulerAngles;

            transform.LookAt(rotatedFwd + this.transform.position, rotatedUp);       
        }

        private float getAngle(Vector3 a, Vector3 b)
        {
            float cosA = Vector3.Dot(a, b) / a.magnitude * b.magnitude;
            return Mathf.Acos(cosA);
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position, transform.position + rotatedFwd * 15f);
            Gizmos.color = Color.yellow;
            Gizmos.DrawLine(transform.position, transform.position + rotatedUp * 10f);

            Gizmos.color = Color.cyan;
            // distance between Attractor and Imitator
           // Gizmos.DrawLine(this.transform.position, attractor.transform.position);

            Gizmos.color = Color.grey;
            // Attractor's distance from Imitator projected on Imitator's forward
            float attractorHeight = Vector3.Dot(attractor.transform.position, this.transform.up);
            //Gizmos.DrawLine(this.transform.position, this.transform.position + attractorHeight * this.transform.up);
            Vector3 projOnImitator = this.transform.position + (attractor.transform.position - attractorHeight * this.transform.up);
           // Gizmos.DrawLine(this.transform.position, projOnImitator);
          //  Gizmos.DrawLine(projOnImitator, attractor.transform.position);

            Gizmos.color = Color.green;
            // forward normals 
            Gizmos.DrawLine(transform.position, transform.position + transform.forward * 1f); // imitator fwd
            Gizmos.DrawLine(attractor.transform.position, attractor.transform.position + attractor.transform.forward * 1f); //attractor fwd
        }

    }

}
